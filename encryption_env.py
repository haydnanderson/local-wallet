from Crypto.Cipher import PKCS1_OAEP
from Crypto.PublicKey import RSA
from Crypto.Hash import SHA256
import os
import ast
import base64

private_key = RSA.generate(1024)
public_key = private_key.publickey()

export = public_key.exportKey("PEM")
print(export.decode("utf-8"))
#
#
# priv_key = private_key.exportKey("PEM")
# print(priv_key)
# priv_key = priv_key.decode("utf-8")
# print(priv_key)
# os.environ['priv_key'] = priv_key
#
# pub_key = public_key.exportKey("PEM")
# pub_key = pub_key.decode("utf-8")
# os.environ['pub_key'] = pub_key

def decode_rsa(ciphertext, key):
    key = RSA.importKey(key)
    cipher = PKCS1_OAEP.new(key, hashAlgo=SHA256)
    # before decrypt convert the hex string to byte_array
    message = cipher.decrypt(base64.b64decode(ciphertext))
    return message.decode("utf-8")

def encode_rsa(message, key):
    key = RSA.importKey(key)
    cipher = PKCS1_OAEP.new(key)
    ciphertext = cipher.encrypt(message)
    return ciphertext

pub_key = "-----BEGIN PUBLIC KEY-----\nMIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDlOJu6TyygqxfWT7eLtGDwajtN\nFOb9I5XRb6khyfD1Yt3YiCgQWMNW649887VGJiGr/L5i2osbl8C9+WJTeucF+S76\nxFxdU6jE0NQ+Z+zEdhUTooNRaY5nZiu5PgDB0ED/ZKBUSLKL7eibMxZtMlUDHjm4\ngwQco1KRMDSmXSMkDwIDAQAB\n-----END PUBLIC KEY-----"

doge = "-----BEGIN RSA PRIVATE KEY-----\nMIICXQIBAAKBgQDlOJu6TyygqxfWT7eLtGDwajtNFOb9I5XRb6khyfD1Yt3YiCgQ\nWMNW649887VGJiGr/L5i2osbl8C9+WJTeucF+S76xFxdU6jE0NQ+Z+zEdhUTooNR\naY5nZiu5PgDB0ED/ZKBUSLKL7eibMxZtMlUDHjm4gwQco1KRMDSmXSMkDwIDAQAB\nAoGAfY9LpnuWK5Bs50UVep5c93SJdUi82u7yMx4iHFMc/Z2hfenfYEzu+57fI4fv\nxTQ//5DbzRR/XKb8ulNv6+CHyPF31xk7YOBfkGI8qjLoq06V+FyBfDSwL8KbLyeH\nm7KUZnLNQbk8yGLzB3iYKkRHlmUanQGaNMIJziWOkN+N9dECQQD0ONYRNZeuM8zd\n8XJTSdcIX4a3gy3GGCJxOzv16XHxD03GW6UNLmfPwenKu+cdrQeaqEixrCejXdAF\nz/7+BSMpAkEA8EaSOeP5Xr3ZrbiKzi6TGMwHMvC7HdJxaBJbVRfApFrE0/mPwmP5\nrN7QwjrMY+0+AbXcm8mRQyQ1+IGEembsdwJBAN6az8Rv7QnD/YBvi52POIlRSSIM\nV7SwWvSK4WSMnGb1ZBbhgdg57DXaspcwHsFV7hByQ5BvMtIduHcT14ECfcECQATe\naTgjFnqE/lQ22Rk0eGaYO80cc643BXVGafNfd9fcvwBMnk0iGX0XRsOozVt5Azil\npsLBYuApa66NcVHJpCECQQDTjI2AQhFc1yRnCU/YgDnSpJVm1nASoRUnU8Jfm3Oz\nuku7JUXcVpt08DFSceCEX9unCuMcT72rAQlLpdZir876\n-----END RSA PRIVATE KEY-----"

#
test = "Bdykpdq7amKJs0oO13q81kikmai92wXOV1BYgDfJrM3Hj7mLQ2DR1o5kVMv5xKKXBfUqktvu+oHyjhKbzQhFEIVeaH6QYYn1DByFwkrbGtU/6WKtGdT62y7uSUBvnBg5SgA8ZxDeS5bMiOuzn/BLqa0om/N6fBVJcP6Dfee6xQE="
encrypted = "Y/w3pJTXl2PNrphsLaVJrs9W6gZRqnCfEuplbilSP4lNoCv0KhQbWzUYKRY/ga7SYbFt0mpV8Jf1roFR9SwvTbthTlT1xtambmrrtWvbXj03N4uDdBt2Pl129FK4G0FUfJogBkcLzi17/qVdk4zDc8UMKYUiLVCCfRgJJmSiK3I="
# new_test = "ZWhiyev07EQaHahsWKPTKLrxPrkJgiiAwTYg43ves4AQwhdeL0gXQK48R82q/l4w74OzCdCax3W8+QptvZj1VCOHpszgP1/90wIEuqz80NVJmwlVKrwxXua4Vw6Dy+9YSU84SnZzqyjl/ypzZs5bKUJ9QXAcZdoO5GtEBrcIcto="

# print(decode_rsa(test, doge))
# print(encode_rsa(b"admin", pub_key))





# # encrypted = "({},)".format(encrypted)
# print(decode_rsa(encrypted, doge))
# encrypted = (base64.b64decode(encrypted))
# # print(encrypted)
# encrypted = "({},)".format(encrypted)
# # print(encrypted)
# decrypted = key.decrypt(ast.literal_eval(str(encrypted)))
# # print((decrypted.encode()))
# print(decrypted)
# print(str(decrypted.decode("utf-8") ))
