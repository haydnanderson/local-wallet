import os
basedir = os.path.abspath(os.path.dirname(__file__))

class Config(object):
    SECRET_KEY = os.environ.get('SECRET_KEY') or 'you-will-never-guess'
    # ...
    SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL') or \
        'sqlite:///' + os.path.join(basedir, 'databases/app.db')

    # SQLALCHEMY_BINDS = {
    #                     "exchanges": 'sqlite:///' + os.path.join(basedir, 'databases/exchanges.db'),
    #                     "transactions": 'sqlite:///' + os.path.join(basedir, 'databases/transactions.db')
    #                     }
    SQLALCHEMY_TRACK_MODIFICATIONS = True
