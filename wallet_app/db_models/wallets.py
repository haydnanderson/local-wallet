from flask_wtf import FlaskForm
from flask import Flask, make_response, render_template, session, url_for, flash
from wtforms import StringField, PasswordField, BooleanField, SubmitField
from wtforms.validators import DataRequired
from wtforms.validators import ValidationError, DataRequired, Email, EqualTo
from flask_login import LoginManager, UserMixin


# from application import db, login
from wallet_app import db, login
# from application import db


from werkzeug.security import generate_password_hash, check_password_hash
import datetime

class Wallet(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    owner = db.Column(db.Integer, db.ForeignKey('user.id'))
    address = db.Column(db.String(64), index=True, unique=True)
    priv_key = db.Column(db.String(200), index=True, unique=True)
    status = db.Column(db.String(32), default="Active")
    wallet_balance = db.Column(db.Integer, default=0)
    coin = db.Column(db.String(12), default="BTC")

class Transaction(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    coin = db.Column(db.String(12), default="BTC")
    tx_owner = db.Column(db.Integer, db.ForeignKey('user.id'))
    txid = db.Column(db.String(128), index=True, unique=True)
    direction = db.Column(db.String(12), default="Incoming")
    tx_value = db.Column(db.Integer, default=0)
    timestamp = db.Column(db.DateTime, index=True, default=datetime.datetime.now())
