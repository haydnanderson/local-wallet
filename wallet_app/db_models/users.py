from flask_wtf import FlaskForm
from flask import Flask, make_response, render_template, session, url_for, flash
from wtforms import StringField, PasswordField, BooleanField, SubmitField
from wtforms.validators import DataRequired
from wtforms.validators import ValidationError, DataRequired, Email, EqualTo
from flask_login import LoginManager, UserMixin


# from application import db, login
from wallet_app import db, login
# from application import db


from werkzeug.security import generate_password_hash, check_password_hash
import datetime

class User(UserMixin, db.Model):

    def set_password(self, password):
        self.password_hash = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.password_hash, password)

    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(64), index=True, unique=True)
    email = db.Column(db.String(120), index=True, unique=True)
    timestamp = db.Column(db.DateTime, index=True, default=datetime.datetime.now())
    password_hash = db.Column(db.String(1000), index=True, unique=False)
    permissions = db.Column(db.String(12), default=None)
    admin = db.Column(db.Boolean, default=None)


    auth_hash = db.Column(db.String(100), default=None) # Baseline to check surrogate key
    locking_key = db.Column(db.String(400), default=None) # Encrypted surrogate key
    locking_key_salt = db.Column(db.String(400), default=None)

    wallets = db.relationship('Wallet', backref='user', lazy='dynamic')
    transactions = db.relationship('Transaction', backref='user', lazy='dynamic')
#     password_hash = db.Column(db.String(128))

    def __repr__(self):
        return '<User {}>'.format(self.username)
