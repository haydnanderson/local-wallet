from binascii import a2b_base64, hexlify
from os import urandom
import codecs
import hashlib
import ecdsa


#  /$$$$$$$                           /$$                       /$$     /$$
# | $$__  $$                         | $$                      | $$    |__/
# | $$  \ $$ /$$$$$$   /$$$$$$   /$$$$$$$ /$$   /$$  /$$$$$$$ /$$$$$$   /$$  /$$$$$$  /$$$$$$$
# | $$$$$$$//$$__  $$ /$$__  $$ /$$__  $$| $$  | $$ /$$_____/|_  $$_/  | $$ /$$__  $$| $$__  $$
# | $$____/| $$  \__/| $$  \ $$| $$  | $$| $$  | $$| $$        | $$    | $$| $$  \ $$| $$  \ $$
# | $$     | $$      | $$  | $$| $$  | $$| $$  | $$| $$        | $$ /$$| $$| $$  | $$| $$  | $$
# | $$     | $$      |  $$$$$$/|  $$$$$$$|  $$$$$$/|  $$$$$$$  |  $$$$/| $$|  $$$$$$/| $$  | $$
# |__/     |__/       \______/  \_______/ \______/  \_______/   \___/  |__/ \______/ |__/  |__/




def base58(address_hex):
    alphabet = '123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz'
    b58_string = ''
    # Get the number of leading zeros and convert hex to decimal
    leading_zeros = len(address_hex) - len(address_hex.lstrip('0'))
    # Convert hex to decimal
    address_int = int(address_hex, 16)
    # Append digits to the start of string
    while address_int > 0:
        digit = address_int % 58
        digit_char = alphabet[digit]
        b58_string = digit_char + b58_string
        address_int //= 58
    # Add '1' for each 2 leading zeros
    ones = leading_zeros // 2
    for one in range(ones):
        b58_string = '1' + b58_string
    return b58_string

# print(rand)

def big_integer():
    order_bits = 0
    order = 0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEBAAEDCE6AF48A03BBFD25E8CD0364141
    curve_q = order
    while order > 0:
        # print(order)
        order >>= 1
        order_bits += 1

    # print(order_bits)

    order_bytes = (order_bits + 7) // 8  # urandom only takes bytes
    extra_bits = order_bytes * 8 - order_bits  # bits to shave off after getting bytes

    rand = int(hexlify(urandom(order_bytes)), 16)
    rand >>= extra_bits

    # no modding by group order or we'll introduce biases
    while rand >= curve_q:
        rand = int(hexlify(urandom(order_bytes)), 16)
        rand >>= extra_bits
    return rand

def _integer_to_privateKey(big_int):
    big_int = big_int % (int('FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEBAAEDCE6AF48A03BBFD25E8CD0364141', 16) - 1)
    big_int = big_int + 1 # key > 0
    key = hex(big_int)[2:]
    # Add leading zeros if the hex key is smaller than 64 chars
    key = key.zfill(32 * 2)
    return key


def __private_to_public(private_key):
    private_key_bytes = codecs.decode(private_key, 'hex')
    # Get ECDSA public key
    key = ecdsa.SigningKey.from_string(private_key_bytes, curve=ecdsa.SECP256k1).verifying_key
    key_bytes = key.to_string()
    key_hex = codecs.encode(key_bytes, 'hex')
    # Add bitcoin byte
    bitcoin_byte = b'04'
    public_key = bitcoin_byte + key_hex
    return public_key
    # print(public_key)

def _public_to_address(public_key, testnet=False, script_hash=False):
    public_key_bytes = codecs.decode(public_key, 'hex')
    # Run SHA256 for the public key
    sha256_bpk = hashlib.sha256(public_key_bytes)
    sha256_bpk_digest = sha256_bpk.digest()
    # Run ripemd160 for the SHA256
    ripemd160_bpk = hashlib.new('ripemd160')
    ripemd160_bpk.update(sha256_bpk_digest)
    ripemd160_bpk_digest = ripemd160_bpk.digest()
    ripemd160_bpk_hex = codecs.encode(ripemd160_bpk_digest, 'hex')
    # Add network byte
    if(testnet == True):
        # https://en.bitcoin.it/wiki/List_of_address_prefixes
        if(script_hash):
            # script hash
            network_byte = b'C4'
        else:
            # pubkey
            network_byte = b'6F'
    else:
        network_byte = b'00'
    network_bitcoin_public_key = network_byte + ripemd160_bpk_hex
    network_bitcoin_public_key_bytes = codecs.decode(network_bitcoin_public_key, 'hex')
    # Double SHA256 to get checksum
    sha256_nbpk = hashlib.sha256(network_bitcoin_public_key_bytes)
    sha256_nbpk_digest = sha256_nbpk.digest()
    sha256_2_nbpk = hashlib.sha256(sha256_nbpk_digest)
    sha256_2_nbpk_digest = sha256_2_nbpk.digest()
    sha256_2_hex = codecs.encode(sha256_2_nbpk_digest, 'hex')
    checksum = sha256_2_hex[:8]
    # Concatenate public key and checksum to get the address
    address_hex = (network_bitcoin_public_key + checksum).decode('utf-8')
    wallet = base58(address_hex)
    return wallet

def generate_wallet(testnet=True):
    key = _integer_to_privateKey(big_integer())
    public_key = __private_to_public(key)
    wallet = _public_to_address(public_key, testnet=testnet)
    return (wallet, key)

if __name__ == "__main__":
    big_int = big_integer()
    priv = _integer_to_privateKey(big_int)
    print(priv)

    # print(codecs.encode(priv, 239))
    print(_public_to_address(__private_to_public(priv), testnet=True))
#     import time
#     start_time = time.time()
#     address, private_key = generate_wallet()
#     print("Private Key: {}".format(private_key))
#     print("Wallet Address: {}".format(address))
#     print("--- %s seconds ---" % (time.time() - start_time))
