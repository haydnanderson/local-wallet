from urllib.request import urlopen, Request
import requests
import json

def get_valid_json(request, allow_204=False):
    if request.status_code == 429:
        raise RateLimitError('Status Code 429', request.text)
    elif request.status_code == 204 and allow_204:
        return True
    try:
        return request.json()
    except JSONError as error:
        msg = 'JSON deserialization failed: {}'.format(str(error))
    raise requests.exceptions.ContentDecodingError(msg)


def base_endpoint(url, data=None):
    req = Request(
        url,
        data=None,
        headers={
            'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.47 Safari/537.36'
        }
    )
    f = urlopen(req).read()
    data = json.loads(f.decode("utf-8"))
    # print(data)
    return(data)

def get_tx(*args, coin="btc", testnet=True):
    # print(args)
    param = ";".join(str(e) for e in args[0])

    if(coin == "btc"):
        if(testnet):
            subset = "test3"
        else:
            subset = "main"
    url = "https://api.blockcypher.com/v1/{coin}/{subset}/addrs/{address}".format(coin=coin, subset=subset, address=param)
    r = base_endpoint(url)
    return r

def pushtx(tx, coin="btc", testnet=True):
    if(coin == "btc"):
        if(testnet):
            subset = "test3"
        else:
            subset = "main"
    data = {
        "tx":tx
    }
    r = requests.post("https://api.blockcypher.com/v1/btc/test3/txs/push", json=data)
    return(get_valid_json(r))
    # return r

def get_address_balance(*args, coin="btc", testnet=True):
    """
    Pass all the addresses in as the args
    """
    if(len(args) > 1):
        param = ";".join(args)
    else:
        param = args[0]

    if(coin == "btc"):
        if(testnet):
            subset = "test3"
        else:
            subset = "main"
    url = "https://api.blockcypher.com/v1/{coin}/{subset}/addrs/{address}/balance".format(coin=coin, subset=subset, address=param)
    r = base_endpoint(url)
    return r

def estimate_kb(inputs=1, outputs=2):
    # in*180 + out*34 + 10 plus or minus 'in'
    # https://bitcoin.stackexchange.com/questions/1195/how-to-calculate-transaction-size-before-sending-legacy-non-segwit-p2pkh-p2sh
    kb = (inputs*180)+(outputs*34) +10
    return ((kb-inputs)/1000, (kb+inputs)/1000)

def btc_per_kb(type="high", network="test3"):
    """
    type = ['high', "med", "low", "all"]
    """
    # http://api.blockcypher.com/v1/btc/test3
    url = "http://api.blockcypher.com/v1/btc/{}".format(network)
    response = urlopen(url).read()
    data = json.loads(response)
    if(type == "all"):
        kb_data = dict()
        kb_data['high'] = data['high_fee_per_kb']
        kb_data['med'] = data['medium_fee_per_kb']
        kb_data['low'] = data['low_fee_per_kb']
        return kb_data
    elif(type == "high"):
        return data['high_fee_per_kb']
    elif(type == "med"):
        return data['medium_fee_per_kb']
    elif(type == "low"):
        return data['low_fee_per_kb']


def estimate_fee(inputs=1, outputs=2, speed="high", network="test3"):
    # 0.00011000
    kb_price = btc_per_kb(type=speed, network=network)
    kb = estimate_kb(inputs=inputs, outputs=outputs)
    avg_kb = ((kb[0]+kb[1])/2)
    # print(kb_price/1e8)
    return int(avg_kb*kb_price)
