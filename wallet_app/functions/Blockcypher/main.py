from urllib.request import urlopen, Request
import requests
import json

class Blockcypher:

    supported_coins = {
        "BTC": [
            "main",
            "test3"
        ],
        "DASH": [
            "main"
        ],
        "DOGE": [
            "main"
        ],
        "LTC": [
            "main"
        ]
    }
    endpoints = {

      "txs": {
        "base": {
          "supported_coins": ['BTC', "BTCTEST"],
          "type": "GET",
          "data": "$TXHASH"
        },
        "push":{
          "supported_coins": ['BTC', "BTCTEST"],
          "type": "POST",
          "data": "$RAWTX"
        },
        "confidence": {
          "supported_coins": ['BTC', "BTCTEST"],
          "type": "GET",
          "data": "$TXHASH",
          "end_cap": "/confidence"
        }
      },
      "addrs": {
        "base": {
          "supported_coins": ['BTC', "BTCTEST", "ETH"],
          "type": "GET",
          "data": "ADDRESS"
        },
        "balance": {
          "supported_coins": ['BTC', "BTCTEST", "ETH"],
          "type": "GET",
          "data": "$ADDRESS",
          "end_cap": "/balance"
        },
        "full": {
          "supported_coins": ['BTC', "BTCTEST"],
          "type": "GET",
          "data": "$ADDRESS",
          "end_cap": "/full"
        }
      }
    }

    headers = {
        'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.47 Safari/537.36'
    }

    def __init__(self, api_key=None, coin="BTC", subset="main"):
        self.api_key = api_key

        if(coin in self.supported_coins):
            if(subset in self.supported_coins[coin]):
                self.coin = coin.lower()
                self.subset = subset.lower()
            else:
                # TODO: Assert error
                print("Fail")
        else:
            # TODO: Assert error here
            print("Fail")
        self.base_url = "https://api.blockcypher.com/v1/{coin}/{chain}/".format(coin=self.coin, chain=self.subset)

    def get_valid_json(self,request, allow_204=False):
        if request.status_code == 429:
            raise RateLimitError('Status Code 429', request.text)
        elif request.status_code == 204 and allow_204:
            return True
        try:
            return request.json()
        except JSONError as error:
            msg = 'JSON deserialization failed: {}'.format(str(error))
        raise requests.exceptions.ContentDecodingError(msg)

    def build_url(self, *args, data=None):
        """
        Pass the variables in as an argument
        param data: for post requests
        """
        args, endpoint, subpoint = args[:-2][0],args[-2],args[-1]
        if(len(args) > 1):
            param = ";".join(str(e) for e in args)
        else:
            param = args[0]

        if(endpoint in self.endpoints):
            if(subpoint in self.endpoints[endpoint]):
                # Code to check if coin is supported
                if(self.endpoints[endpoint][subpoint]['type'] == "POST"):
                    return("{endpoint}/{subpoint}".format(endpoint=endpoint, subpoint=subpoint))
                if("end_cap" in self.endpoints[endpoint][subpoint]):
                    end_cap = self.endpoints[endpoint][subpoint]['end_cap']
                    return("{endpoint}/{param}{endcap}".format(endpoint=endpoint, param=param, endcap=end_cap))
                else:
                    # print(param)
                    return("{endpoint}/{param}".format(endpoint=endpoint, param=param))

    def base_endpoint(self, url, data=None, post=False):
        if(not post):
            req = requests.get(
                url,
                data=None,
                headers=self.headers
            )
        else:
            req = requests.post(
                url,
                json=data,
                headers=self.headers
            )
        # print(req.status_code)
        data = self.get_valid_json(req)
        # print(data)
        return(data)

    def get_tx(self, *args, include_balance=True):
        """
        Pass the addresses in as args for transaction retrieval
        """
        response = dict((address, {"final_balance":None,"txrefs":{},"unconfirmed_txrefs":{}}) for address in args)

        # url = "https://api.blockcypher.com/v1/{coin}/{subset}/addrs/{address}".format(coin=self.coin, subset=self.subset, address=param)
        url = self.base_url+self.build_url(args, "addrs", "base")
        print(url)
        r = self.base_endpoint(url)

        # If more than one address provided in arguments
        if(len(args) > 1):
            for i in r:
                if(include_balance):
                    response[i['address']]['final_balance'] = i['final_balance']
                if("txrefs" in i):
                    response[i['address']]['txrefs'] = i['txrefs']
                if("unconfirmed_txrefs" in i):
                    response[i['address']]['unconfirmed_txrefs'] = i['unconfirmed_txrefs']
        else:
            if(include_balance):
                response[r['address']]['final_balance'] = r['final_balance']
            if("txrefs" in r):
                response[r['address']]['txrefs'] = r['txrefs']
            if("unconfirmed_txrefs" in r):
                response[r['address']]['unconfirmed_txrefs'] = r['unconfirmed_txrefs']

        return response

    def push_tx(self, tx):
        data = {
            "tx": tx
        }

        # url = "https://api.blockcypher.com/v1/{coin}/{chain}/txs/push".format(coin=self.coin, chain=self.subset)
        url = self.base_url+self.build_url(tx, "txs", "push")
        r = self.base_endpoint(url, post=True, data=data)
        return(r)

    def get_address_balance(self, *args):
        url = self.base_url+self.build_url(args, "addrs", "balance")
        r = self.base_endpoint(url)
        return r


#
# def get_valid_json(request, allow_204=False):
#     if request.status_code == 429:
#         raise RateLimitError('Status Code 429', request.text)
#     elif request.status_code == 204 and allow_204:
#         return True
#     try:
#         return request.json()
#     except JSONError as error:
#         msg = 'JSON deserialization failed: {}'.format(str(error))
#     raise requests.exceptions.ContentDecodingError(msg)
#
#
# def base_endpoint(url, data=None):
#     req = Request(
#         url,
#         data=None,
#         headers={
#             'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.47 Safari/537.36'
#         }
#     )
#     f = urlopen(req).read()
#     data = json.loads(f.decode("utf-8"))
#     # print(data)
#     return(data)
#
# def get_tx(*args, coin="btc", testnet=True):
#     # print(args)
#     param = ";".join(str(e) for e in args[0])
#
#     if(coin == "btc"):
#         if(testnet):
#             subset = "test3"
#         else:
#             subset = "main"
#     url = "https://api.blockcypher.com/v1/{coin}/{subset}/addrs/{address}".format(coin=coin, subset=subset, address=param)
#     r = base_endpoint(url)
#     return r
#
# def pushtx(tx, coin="btc", testnet=True):
#     if(coin == "btc"):
#         if(testnet):
#             subset = "test3"
#         else:
#             subset = "main"
#     data = {
#         "tx":tx
#     }
#     r = requests.post("https://api.blockcypher.com/v1/btc/test3/txs/push", json=data)
#     return(get_valid_json(r))
#     # return r
#
# def get_address_balance(*args, coin="btc", testnet=True):
#     """
#     Pass all the addresses in as the args
#     """
#     if(len(args) > 1):
#         param = ";".join(args)
#     else:
#         param = args[0]
#
#     if(coin == "btc"):
#         if(testnet):
#             subset = "test3"
#         else:
#             subset = "main"
#     url = "https://api.blockcypher.com/v1/{coin}/{subset}/addrs/{address}/balance".format(coin=coin, subset=subset, address=param)
#     r = base_endpoint(url)
#     return r
#
# def estimate_kb(inputs=1, outputs=2):
#     # in*180 + out*34 + 10 plus or minus 'in'
#     # https://bitcoin.stackexchange.com/questions/1195/how-to-calculate-transaction-size-before-sending-legacy-non-segwit-p2pkh-p2sh
#     kb = (inputs*180)+(outputs*34) +10
#     return ((kb-inputs)/1000, (kb+inputs)/1000)
#
# def btc_per_kb(type="high", network="test3"):
#     """
#     type = ['high', "med", "low", "all"]
#     """
#     # http://api.blockcypher.com/v1/btc/test3
#     url = "http://api.blockcypher.com/v1/btc/{}".format(network)
#     response = urlopen(url).read()
#     data = json.loads(response)
#     if(type == "all"):
#         kb_data = dict()
#         kb_data['high'] = data['high_fee_per_kb']
#         kb_data['med'] = data['medium_fee_per_kb']
#         kb_data['low'] = data['low_fee_per_kb']
#         return kb_data
#     elif(type == "high"):
#         return data['high_fee_per_kb']
#     elif(type == "med"):
#         return data['medium_fee_per_kb']
#     elif(type == "low"):
#         return data['low_fee_per_kb']
#
#
# def estimate_fee(inputs=1, outputs=2, speed="high", network="test3"):
#     # 0.00011000
#     kb_price = btc_per_kb(type=speed, network=network)
#     kb = estimate_kb(inputs=inputs, outputs=outputs)
#     avg_kb = ((kb[0]+kb[1])/2)
#     # print(kb_price/1e8)
#     return int(avg_kb*kb_price)
