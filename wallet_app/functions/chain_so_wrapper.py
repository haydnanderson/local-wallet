from urllib.request import urlopen, Request
import json

def base_endpoint(url):
    req = Request(
        url,
        data=None,
        headers={
            'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.47 Safari/537.36'
        }
    )
    f = urlopen(req).read()
    data = json.loads(f.decode("utf-8"))
    # print(data)
    return(data)

def get_last_transaction(address, network="BTCTEST"):
    url = "https://chain.so/api/v2/get_tx_received/{}/{}".format(network, address)
    data = base_endpoint(url)
    # print(data['data'])
    return data['data']['txs'][-1]['txid']

def check_btc_balance(address, confirmations=2, network="BTCTEST"):
    url = "https://chain.so/api/v2/get_address_balance/{network}/{}/{}".format(address, confirmations, network=network)
    data = base_endpoint(url)
    return data['data']

def pushtx(tx, network):
    url = "https://chain.so/api/v2/send_tx/{network}"
    data = {

    }

if __name__ == "__main__":
    print(get_last_transaction("mn5M8EX6AJmLGQhcWzHPHitm3MrgRqdUEj"))
