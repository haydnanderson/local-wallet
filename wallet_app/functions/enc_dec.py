from Crypto.Cipher import PKCS1_OAEP
from Crypto.PublicKey import RSA
from Crypto.Hash import SHA256
from Crypto.Cipher import AES
import os
import ast
import base64


from itertools import cycle
import base64

def xore(data, key):
    return bytes(a ^ b for a, b in zip(data, cycle(key)))

def decode_rsa(ciphertext, key):
    key = RSA.importKey(key)
    cipher = PKCS1_OAEP.new(key, hashAlgo=SHA256)
    # before decrypt convert the hex string to byte_array
    message = cipher.decrypt(base64.b64decode(ciphertext))
    return message.decode("utf-8")

def encode_rsa(message, key):
    key = RSA.importKey(key)
    cipher = PKCS1_OAEP.new(key)
    ciphertext = cipher.encrypt(message)
    return ciphertext

class AESCipher(object):
    def __init__(self, key):
        self.bs = 16
        self.cipher = AES.new(key, AES.MODE_ECB)

    def encrypt(self, raw):
        raw = self._pad(raw)
        encrypted = self.cipher.encrypt(raw.encode("utf8"))
        encoded = base64.b64encode(encrypted)
        return str(encoded, 'utf-8')

    def decrypt(self, raw):
        decoded = base64.b64decode(raw)
        decrypted = self.cipher.decrypt(decoded)
        return str(self._unpad(decrypted), 'utf-8')

    def _pad(self, s):
        return s + (self.bs - len(s) % self.bs) * chr(self.bs - len(s) % self.bs)

    def _unpad(self, s):
        return s[:-ord(s[len(s)-1:])]
