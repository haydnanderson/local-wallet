from flask import Flask, g
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_restful import Resource, Api
from flask_login import LoginManager, UserMixin
from werkzeug.security import generate_password_hash, check_password_hash
from flask_login import current_user, login_user, logout_user, login_required


from Crypto.Cipher import PKCS1_OAEP
from Crypto.PublicKey import RSA
from Crypto.Hash import SHA256
import os
import ast
import base64


"""
Local imports
"""
from wallet_app.config import Config

app = Flask(__name__)
app.secret_key = '5accdb11b2c10a78d7c92c5fa102ea77fcd50c2058b00f6e'
app.config.from_object(Config)
db = SQLAlchemy(app)
login = LoginManager(app)
login.login_view = 'login'
# db.create_all()
migrate = Migrate(app, db)

private_key = RSA.generate(1024)
public_key = private_key.publickey()

priv_key = private_key.exportKey("PEM").decode("utf-8")
os.environ['priv_key'] = priv_key

pub_key = public_key.exportKey("PEM").decode("utf-8")
os.environ['pub_key'] = pub_key

print(priv_key)
print(pub_key)

from wallet_app import routes
from wallet_app import forms
