from wallet_app import app, login, db
from wallet_app.db_models.users import User
from wallet_app.db_models.wallets import Wallet, Transaction
from wallet_app.functions.Blockcypher import Blockcypher
from wallet_app.functions.enc_dec import *
from wallet_app.functions import misc
from wallet_app.functions.generate_addresses import generate_wallet
# from wallet_app.functions.chain_so_wrapper import check_btc_balance
from wallet_app.functions.blockcypher_wrapper import estimate_fee, pushtx, get_address_balance, get_tx
from flask import render_template, Flask, render_template, flash, redirect, request, jsonify, abort, url_for
from flask_login import current_user, login_user, logout_user, login_required
# from wallet_app import l
from datetime import datetime
from cryptos import *
import os
from pbkdf2 import PBKDF2
from Crypto.Cipher import AES
import base64
import os
import sys
import hashlib
import random
import string

supported_coins = ["BTC", "BTCTEST"]

@login.user_loader
def load_user(id):
    return User.query.get(int(id))

@app.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('login'))

@app.route('/')
@login_required
def index():
    return render_template("index.html")

@app.route('/login')
def login():
    if current_user.is_authenticated:
        return redirect(url_for('index'))

    if(request.args.get("username") != None and request.args.get("password") != None):
        user = User.query.filter_by(username=request.args.get("username")).first()
        if user is None or not user.check_password(request.args.get("password")):
            return redirect(url_for('login'))
        else:
            login_user(user)
            return redirect(url_for('index'))

    return render_template("login.html")

@app.route('/register')
def register():
    if current_user.is_authenticated:
        return redirect(url_for('index'))

    """
    Random key to encrypt everything
    """
    random_string = ''.join(random.choices(string.ascii_uppercase + string.digits+string.ascii_lowercase, k=25))
    salt = str.encode(hashlib.sha256(random_string.encode("utf-8")).hexdigest())
    key = PBKDF2(random_string, os.urandom(8)).read(32) # 256-bit key
    surrogate_key = key # Encrypt all data with this key

    encrypted_username = AESCipher(surrogate_key)
    auth_hash = encrypted_username.encrypt(request.args.get("username")) # Store this
    """
    Calculate database stored values
    """
    salt = os.urandom(8)
    db_salt = base64.b64encode(salt).decode('utf8') # Store this
    locking_key = PBKDF2(request.args.get("password"), salt).read(32) # User should generate this every time
    stored_value = xore(surrogate_key, locking_key)
    b64_stored_value = base64.b64encode(stored_value).decode("utf-8") # Store the b64 encoded value

    user = User(auth_hash=auth_hash, locking_key=b64_stored_value, locking_key_salt=db_salt, username=request.args.get("username"), email=request.args.get("email"))
    user.set_password(request.args.get("password"))
    db.session.add(user)
    db.session.commit()
    return redirect(url_for('index'))

@app.route("/server_public")
def server_pub():
    response = {
        "public_key": os.environ['pub_key']
    }
    return jsonify(response)

@app.route("/test_encryption")
def encrypt_test():
    encrypted_data = request.args.get("encrypted_data").replace(" ", "+") # Encrypted password
    try:
        decoded = decode_rsa(encrypted_data, os.environ['priv_key'])
        print(decoded)
        response = {
            "status": True,
            "msg": "Encryption working"
        }
    except Exception as e:
        print(e)
        response = {
            "status": False,
            "msg": "Encryption failure"

        }

    return jsonify(response)





@app.route('/add_address')
@login_required
def add_address():
    user = db.session.query(User).filter(User.id == current_user.id).first()
    coin = request.args.get("coin").upper()
    encrypted_data = request.args.get("encrypted_data").replace(" ", "+")
    # print(encrypted_data)
    pwd = decode_rsa(encrypted_data, os.environ['priv_key'])




    user = db.session.query(User).filter(User.id == current_user.id).first()
    if(coin not in supported_coins):
        return abort(404)

    if(coin == "BTCTEST"):
        address, key = generate_wallet(testnet=True)
    elif(coin == "BTC"):
        address, key = generate_wallet(testnet=False)

    # print(pwd, key)
    encrypted = misc.encrypt_text(user, pwd, key)
    wallet = Wallet(user=user, address=address, priv_key=encrypted, coin=coin)

    db.session.add(wallet)
    db.session.commit()

    response = {
        "address": address,
        "status": wallet.status,
        "balance": wallet.wallet_balance,
        "coin": coin
    }
    # response = {
    #     "address": "TESTING",
    #     "status": "TESTING",
    #     "balance": "TESTING",
    #     "coin": coin
    # }
    return jsonify(response)


@app.route("/wallet/<coin>")
@login_required
def wallet(coin):
    if(coin.upper() not in supported_coins):
        return abort(404)
    user = db.session.query(User).filter(User.id == current_user.id).first().id
    addresses = db.session.query(Wallet).filter(Wallet.owner == user, Wallet.coin == coin).all()
    transactions = db.session.query(Transaction).filter(Transaction.tx_owner == user, Transaction.coin == coin).all()
    total = 0
    print(transactions)
    for i in addresses:
        total += i.wallet_balance
    # print(address_list)
    return render_template("index.html",coin=coin, wallets=addresses[::-1], total_balance=round(total,8), transactions=transactions[::-1])

@app.route("/send/<coin>")
@login_required
def send(coin):
    coin = coin.upper()
    if(coin not in supported_coins):
        return abort(404)
    user = db.session.query(User).filter(User.id == current_user.id).first().id
    addresses = db.session.query(Wallet).filter(Wallet.owner == user, Wallet.coin == coin).all()
    return render_template("multisend.html", wallets=addresses, coin=coin)

@app.route("/update_balance/<coin>")
def update_balance(coin):
    address = request.args.get("address")
    coin = coin.upper()
    print(coin)
    user = db.session.query(User).filter(User.id == current_user.id).first()
    if(coin not in supported_coins):
        return abort(404)

    if(coin == "BTC"):
        b = Blockcypher("API", coin="BTC")
    elif(coin == "BTCTEST"):
        b = Blockcypher("API", coin="BTC", subset="test3")



    # network = request.args.get("network")

    balance = b.get_tx(address)
    # print(balance)
    wallet  = db.session.query(Wallet).filter(Wallet.address == address).first()
    wallet.wallet_balance = round(float(balance[address]['final_balance'])/1e8,8)
    db.session.commit()



    for i in balance[address]['txrefs']:
        if(i['tx_input_n'] == -1):
            status = "Incoming"
        else:
            status = "Outgoing"

        # CHeck db if tx exists
        tx = db.session.query(Transaction).filter(Transaction.txid == i['tx_hash']).first()
        if(tx == None):
            value = round(i['value']/1e8, 8)
            timestamp = datetime.strptime(i['confirmed'], '%Y-%m-%dT%H:%M:%SZ')
            new_tx = Transaction(coin=coin, user=user, txid=i['tx_hash'], direction=status,tx_value=value,timestamp=timestamp)
            db.session.add(new_tx)
            db.session.commit()

    for i in balance[address]['unconfirmed_txrefs']:
        tx = db.session.query(Transaction).filter(Transaction.txid == i['tx_hash']).first()
        if(tx == None):
            if(i['tx_input_n'] == -1):
                status = "Incoming"
            else:
                status = "Outgoing"
            value = round(i['value']/1e8, 8)
            timestamp = datetime.strptime(i['received'].split('.')[0], '%Y-%m-%dT%H:%M:%SZ')
            new_tx = Transaction(coin=coin, user=user, txid=i['tx_hash'], direction=status,tx_value=value,timestamp=timestamp)
            db.session.add(new_tx)
            db.session.commit()
    # print(wallet.wallet_balance)
    response = {
        "address": address,
        "status": wallet.status,
        "balance": wallet.wallet_balance
    }
    return jsonify(response)

@app.route("/send_tx/<coin>")
def send_tx(coin):
    coin = coin.upper()
    if(coin not in supported_coins):
        return abort(404)

    user = db.session.query(User).filter(User.id == current_user.id).first()
    if(coin == "BTCTEST"):
        b = Bitcoin(testnet=True)
    elif(coin == "BTC"):
        b = Bitcoin()

    from_address = request.args.get("from")
    to = request.args.get("to_address")
    value = float(request.args.get("value"))*1e8
    tx_speed = request.args.get("speed")

    wallet = db.session.query(Wallet).filter(Wallet.address == from_address).first()

    fee = estimate_fee(outputs=1, speed=tx_speed)

    if(value+fee > (wallet.wallet_balance)*1e8):
        """
        Flash warning here
        """
        flash('Not enough BTC in wallet!', "danger")
        return redirect(url_for("send", coin=coin))

    tx = b.preparesignedtx(wallet.priv_key, to, int(value), fee=fee)
    #Checks if the send address is one of our own
    send_addy = db.session.query(Wallet).filter(Wallet.address == to, Wallet.owner == user.id).first()
    if(send_addy != None):
        # One of our addresses
        print("[!] We own this address")
        send_addy.wallet_balance += round((value)/1e8, 8)
    wallet.wallet_balance -= round((value+fee)/1e8, 8)
    db.session.commit()
    response = pushtx(tx)
    # print(response)

    transaction = Transaction(coin=coin, user=user, txid=response['tx']['hash'], direction="Inter-Wallet", tx_value=round(value/1e8,8))
    db.session.add(transaction)
    db.session.commit()


    print(from_address, to, value, tx_speed)
    flash('Successfully sent {} BTC'.format(round(value/1e8,8)), "success")
    return redirect(url_for("send", coin=coin))
