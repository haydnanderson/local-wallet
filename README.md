Local Wallet
========

What is Local Wallet?
-----------------
Local wallet is a Bitcoin and Bitcoin Testnet wallet. Uses RSA 1024 and AES 256 Encryption to secure the private keys generated. Equipped with the ability to send and recieve from the wallet itself. Dash, Litecoin and Ethereum support is to come in the future.

<p align="center">
  <img src="./img/main.png" alt="Local wallet Bitcoin Test main screen">
</p>

<p align="center">
  <img src="./img/send.png" alt="Local wallet Bitcoin Test send screen">
</p>
<p align="center">
  <img src="./img/refresh.png" alt="Local wallet Bitcoin Test refresh">
</p>
