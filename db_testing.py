from wallet_app import app, db
from wallet_app.db_models.wallets import Wallet, Transaction
from wallet_app.db_models.users import User
from wallet_app.functions.enc_dec import *
from pbkdf2 import PBKDF2
import base64

def b64_bytes(b64code):
    return base64.b64decode(b64code)

def get_auth_hash(user):
    """
    Pass user object to get auth hash
    """
    return user.auth_hash

def get_encrypted_surrogate(user):
    """
    Pass user object to get encrypted surrogate key
    """
    return user.locking_key

def get_encrypted_key_salt(user):
    """
    Pass user object to get encrypted surrogate key
    """
    return user.locking_key_salt

def calculate_surrogate(user, password):
    """
    Calculate surrogate key from password
    """
    salt = get_encrypted_key_salt(user)
    salt_bytes = b64_bytes(salt)
    test = PBKDF2(password, salt_bytes).read(32)

    encrypted_surrogate = b64_bytes(get_encrypted_surrogate(user))
    # print(encrypted)
    claimed_surrogate_key = xore(encrypted_surrogate, test)
    return claimed_surrogate_key

def check_password_surrogate(user, password, claimed_surrogate_key=None):
    """
    Check password against auth hash for user
    """
    if(claimed_surrogate_key == None):
        claimed_surrogate_key = calculate_surrogate(user, password)
    cipher = AESCipher(claimed_surrogate_key)
    if(cipher.encrypt(user.username) == get_auth_hash(user)):
        return True
    else:
        return False

def encrypt_text(user, password, text):
    surrogate = calculate_surrogate(user, password)
    if(check_password_surrogate(user, password, claimed_surrogate_key=surrogate)):
        cipher = AESCipher(surrogate)
        return(cipher.encrypt(text))
    else:
        return False

def decrypt_text(user, password, encrypted_text):
    surrogate = calculate_surrogate(user, password)
    if(check_password_surrogate(user, password, claimed_surrogate_key=surrogate)):
        cipher = AESCipher(surrogate)
        return(cipher.decrypt(encrypted_text))
    else:
        return False


def get_user(username):
    return db.session.query(User).filter(User.username == username).first()


encrypted = "cQ92+k7L/Yf4dZrJiUAggThW6FLAtZfJlLTwQ2xt7PWSbojjVmwiMyB7xi5iAj7iFT/YxBGDIUSLaQH4gnyRFq0o/nO7AVGBxRJwWvkwm+I="



# import os
# encrypted = "oyoXmYMmLN5wmWuqUGOcEz4XT37J10vzsBjwx9bAqbKK3J+3S6Fyco9F+5MA1Crbr5S6v+9isu91qZsIYTkIRpwhjiXrbh+Q/SyMo/K0b1n094mVGvQJL8kMbYbvdISC41zQIJwfnuf5VphflKtIiBl6rfX9/8M6SvUANea3oTE="
# # print(decode_rsa(encrypted, os.environ['priv_key']))
# print(os.environ['priv_key'])

# print(os.environ['priv_key'])
user = get_user("Anonymous")
# encrypted = encrypt_text(user, "anon", "password")

print(decrypt_text(user, "anon", encrypted))
